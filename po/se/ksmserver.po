# Translation of ksmserver to Northern Sami
#
# Børre Gaup <boerre@skolelinux.no>, 2003, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-15 02:12+0000\n"
"PO-Revision-Date: 2005-10-24 00:00+0200\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Sami <l10n-no@lister.huftis.org>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr ""

#: main.cpp:77
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:81 main.cpp:89
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:84
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:91
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:98
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:111 main.cpp:146
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:113 main.cpp:148
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:124 main.cpp:137
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:155
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:162
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:196
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Máhcaha ovddit bargovuoru dili, jos vejolaš"

#: main.cpp:203
#, kde-format
msgid "Also allow remote connections"
msgstr "Divtte gáiddus čatnusiid maid"

#: main.cpp:206
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:210
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:884
#, kde-format
msgid "Session Management"
msgstr ""

#: server.cpp:889
#, kde-format
msgid "Log Out"
msgstr "Olggosčáliheapmi"

#: server.cpp:894
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:899
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:905
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Nannenkeahtes olggosčáliheapmi"

#: server.cpp:910
#, fuzzy, kde-format
#| msgid "Halt Without Confirmation"
msgid "Shut Down Without Confirmation"
msgstr "Bisset dihtora nannenkeahttá"

#: server.cpp:915
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Vuolggat dihtora ođđasit nannenkeahttá"
