# Albanian translation for kdebase-workspace
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdebase-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2009-08-08 18:24+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-05-05 23:18+0000\n"
"X-Generator: Launchpad (build 12959)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgid "Appearance"
msgstr "Paraqitja"

#: contents/ui/analogclock.qml:78
#, kde-format
msgctxt "@info:tooltip"
msgid "Current time is %1; Current date is %2"
msgstr ""

#: contents/ui/configGeneral.qml:23
#, fuzzy, kde-format
#| msgid "Show &seconds hand"
msgid "Show seconds hand"
msgstr "Shfaq &dorën e sekondave"

#: contents/ui/configGeneral.qml:24
#, kde-format
msgid "General:"
msgstr ""

#: contents/ui/configGeneral.qml:28
#, fuzzy, kde-format
#| msgid "Show &time zone"
msgid "Show time zone"
msgstr "Shfaq &zonën kohore"

#~ msgid "Show the seconds"
#~ msgstr "Shfaq sekondat"

#~ msgid "Check this if you want to show the seconds."
#~ msgstr "Përzgjidh këtë nëse do që të shfaqen sekondat."

#~ msgid "Show &seconds hand"
#~ msgstr "Shfaq &dorën e sekondave"

#~ msgid "Show the Timezone in text"
#~ msgstr "Shfaq Zonën Kohore në tekst"

#~ msgid "Check this if you want to display Timezone in text."
#~ msgstr "Përzgjidh këtë nëse do të shfaqësh zonën kohore në tekst."

#~ msgid "Show &time zone"
#~ msgstr "Shfaq &zonën kohore"
