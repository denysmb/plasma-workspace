# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-04 02:12+0000\n"
"PO-Revision-Date: 2023-03-16 20:39+0000\n"
"Last-Translator: gummi <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/ChangePassword.qml:28
#: package/contents/ui/UserDetailsPage.qml:155
#, kde-format
msgid "Change Password"
msgstr "Breyta lykilorði"

#: package/contents/ui/ChangePassword.qml:33
#, kde-format
msgid "Set Password"
msgstr "Velja lykilorð"

#: package/contents/ui/ChangePassword.qml:56
#, kde-format
msgid "Password"
msgstr "Lykilorð"

#: package/contents/ui/ChangePassword.qml:71
#, kde-format
msgid "Confirm password"
msgstr "Staðfesta lykilorð"

#: package/contents/ui/ChangePassword.qml:90
#: package/contents/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Lykilorðin þurfa að stemma"

#: package/contents/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "Breyta lykilorði veskis?"

#: package/contents/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Nú þegar þú hefur breytt lykilorðinu fyrir innskráningu, þá gætirðu viljað "
"breyta einnig lykilorðinu að KWallet-veskinu til samræmis."

#: package/contents/ui/ChangeWalletPassword.qml:30
#, kde-format
msgid "What is KWallet?"
msgstr "Hvaða er KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:40
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet er lykilorðastjóri sem geymir lykilorðin þín fyrir þráðlaus net og "
"aðrar dulritaðar samskiptaleiðir. Því er læst með eigin lykilorði sem er "
"frábrugðið lykilorðinu sem þú notar til að skrá þig inn. Ef þessi tvö "
"lykilorð eru eins er hægt að aflæsa KWallet sjálfkrafa svo þú þurfir ekki að "
"færa inn lykilorðið í KWallet sérstaklega."

#: package/contents/ui/ChangeWalletPassword.qml:56
#, kde-format
msgid "Change Wallet Password"
msgstr "Breyta lykilorði veskis"

#: package/contents/ui/ChangeWalletPassword.qml:65
#, kde-format
msgid "Leave Unchanged"
msgstr "Láta vera óbreytt"

#: package/contents/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Búa til notandaaðgang"

#: package/contents/ui/CreateUser.qml:33
#: package/contents/ui/UserDetailsPage.qml:123
#, kde-format
msgid "Name:"
msgstr "Nafn:"

#: package/contents/ui/CreateUser.qml:37
#: package/contents/ui/UserDetailsPage.qml:130
#, kde-format
msgid "Username:"
msgstr "Notandanafn:"

#: package/contents/ui/CreateUser.qml:47
#: package/contents/ui/UserDetailsPage.qml:138
#, kde-format
msgid "Standard"
msgstr "Staðlað"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:139
#, kde-format
msgid "Administrator"
msgstr "Kerfisstjóri"

#: package/contents/ui/CreateUser.qml:51
#: package/contents/ui/UserDetailsPage.qml:142
#, kde-format
msgid "Account type:"
msgstr "Tegund notandaaðgangs:"

#: package/contents/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Lykilorð:"

#: package/contents/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Staðfesta lykilorð:"

#: package/contents/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Búa til"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Grunnstilla fingraför"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "Hreinsa allt"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Bæta við"

#: package/contents/ui/FingerprintDialog.qml:83
#, kde-format
msgid "Cancel"
msgstr "Hætta við"

#: package/contents/ui/FingerprintDialog.qml:91
#, kde-format
msgid "Done"
msgstr "Lokið"

#: package/contents/ui/FingerprintDialog.qml:115
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Skrái fingrafar"

#: package/contents/ui/FingerprintDialog.qml:124
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr "Ýttu hægri vísifingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr "Ýttu hægri löngutöng nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:128
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr "Ýttu hægri baugfingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:130
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr "Ýttu hægri litla fingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:132
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr "Ýttu hægri þumalfingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:134
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr "Ýttu vinstri vísifingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:136
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr "Ýttu vinstri löngutöng nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:138
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr "Ýttu vinstri baugfingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:140
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr "Ýttu vinstri litla fingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:142
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr "Ýttu vinstri þumalfingri nokkrum sinnum á fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:146
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr "Renndu hægri vísifingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:148
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr "Renndu hægri löngutöng nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:150
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr "Renndu hægri baugfingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:152
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr "Renndu hægri litla fingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:154
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr "Renndu hægri þumalfingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:156
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr "Renndu vinstri vísifingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:158
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr "Renndu vinstri löngutöng nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:160
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr "Renndu vinstri baugfingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:162
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr "Renndu vinstri litla fingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:164
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr "Renndu vinstri þumalfingri nokkrum sinnum yfir fingrafaraskannann."

#: package/contents/ui/FingerprintDialog.qml:179
#, kde-format
msgid "Finger Enrolled"
msgstr "Fingur skráður"

#: package/contents/ui/FingerprintDialog.qml:209
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Veldu fingur sem á að skrá"

#: package/contents/ui/FingerprintDialog.qml:326
#, kde-format
msgid "Re-enroll finger"
msgstr "Skrá fingur aftur"

#: package/contents/ui/FingerprintDialog.qml:333
#, kde-format
msgid "Delete fingerprint"
msgstr "Eyða fingrafari"

#: package/contents/ui/FingerprintDialog.qml:342
#, kde-format
msgid "No fingerprints added"
msgstr "Engum fingraförum bætt við"

#: package/contents/ui/main.qml:19
#, kde-format
msgid "Manage Users"
msgstr "Stjórna notendum"

#: package/contents/ui/main.qml:125
#, kde-format
msgid "Add New User"
msgstr "Bæta við notanda"

#: package/contents/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "Skipta um notandamynd"

#: package/contents/ui/PicturesSheet.qml:21
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "Þetta er ekkert"

#: package/contents/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Flottur Flamingó"

#: package/contents/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Drekaaldin"

#: package/contents/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Sætkartafla"

#: package/contents/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Rafgullið rými"

#: package/contents/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Geislandi geisli"

#: package/contents/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Sítrónulímóna"

#: package/contents/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Grænt og vænt"

#: package/contents/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Grundir og engi"

#: package/contents/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Tepid Teal"

#: package/contents/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasma-blátt"

#: package/contents/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Fjóludúskur"

#: package/contents/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Fjólublátt"

#: package/contents/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Kolamoli"

#: package/contents/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Pappírspési"

#: package/contents/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Kaffibrúnt"

#: package/contents/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Harðviður"

#: package/contents/ui/PicturesSheet.qml:73
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "Velja mynd"

#: package/contents/ui/PicturesSheet.qml:105
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Veldu skrá…"

#: package/contents/ui/UserDetailsPage.qml:106
#, kde-format
msgid "Change avatar"
msgstr "Skipta um notandamynd"

#: package/contents/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Email address:"
msgstr "Netfang:"

#: package/contents/ui/UserDetailsPage.qml:175
#, kde-format
msgid "Delete files"
msgstr "Eyða skrám"

#: package/contents/ui/UserDetailsPage.qml:182
#, kde-format
msgid "Keep files"
msgstr "Halda skrám"

#: package/contents/ui/UserDetailsPage.qml:189
#, kde-format
msgid "Delete User…"
msgstr "Eyða notanda…"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Grunnstilla auðkenningu með fingraförum…"

#: package/contents/ui/UserDetailsPage.qml:214
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Hægt er að nota fingraför í staðinn fyrir lykilorð þegar skjárinn er "
"aflæstur og til að fá stjórnandaaðgang að hugbúnaði og forritum á "
"skipanalínu sem fara fram á slík.<nl/><nl/>Ekki er enn kominn stuðningur við "
"að skrá þig inn í tölvuna með fingrafari."

#: src/fingerprintmodel.cpp:151 src/fingerprintmodel.cpp:258
#, kde-format
msgid "No fingerprint device found."
msgstr "Enginn fingrafaraskanni fannst."

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Retry scanning your finger."
msgstr "Prófaðu að skanna fingurinn þinn aftur."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Strokan var of stutt, reyndu aftur."

#: src/fingerprintmodel.cpp:335
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Fingurinn ekki á miðjum fingrafaralesaranum, prófaðu aftur."

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Fjarlægðu fingurinn af skannanum og reyndu aftur."

#: src/fingerprintmodel.cpp:345
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Skráning fingrafars mistókst."

#: src/fingerprintmodel.cpp:348
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Ekkert pláss eftir á tækinu, eyddu öðrum fingraförum til að halda áfram."

#: src/fingerprintmodel.cpp:351
#, kde-format
msgid "The device was disconnected."
msgstr "Tækið var aftengt."

#: src/fingerprintmodel.cpp:356
#, kde-format
msgid "An unknown error has occurred."
msgstr "Óþekkt villa kom upp."

#: src/user.cpp:279
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Ekki tókst að fá heimild til að vista notandann %1"

#: src/user.cpp:284
#, kde-format
msgid "There was an error while saving changes"
msgstr "Villa kom upp þegar breytingar voru vistaðar"

#: src/user.cpp:380
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr "Mistókst að breyta stærð myndar: tókst ekki að opna bráðabirgðaskrá"

#: src/user.cpp:388
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Mistókst að breyta stærð myndar: tókst ekki að skrifa í bráðabirgðaskrá"

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "Notandareikningurinn þinn"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "Aðrir notendareikningar"

#~ msgid "Continue"
#~ msgstr "Halda áfram"
